﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keeytap
{
    public class DatosUsuario
    {
        public string Nombre_Completo { get; set; }
        public string Edad { get; set; }
        public string Documento { get; set; }
        public string Nombre_Madre { get; set; }
        public string Nombre_Padre { get; set; }
    }
}
