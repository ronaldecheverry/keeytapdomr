﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Keeytap
{
    public class App : Application
    {
        UserData Newusuario;
        public App()
        {
            Newusuario = new UserData
            {
                Nombre = "Ronald",
                Apellido = "Echeverri",
                Documento = "11445879964",
                Edad = 24,
                Peso = "60"
            };

            string claseJSON = JsonConvert.SerializeObject(Newusuario);
            bool guardo = DependencyService.Get<IFileManager>().GuardarTexto(Variables.Nombre_Archivo_Offline, claseJSON);

            bool existe = DependencyService.Get<IFileManager>().Existe(Variables.Nombre_Archivo_Offline);
            if (existe)
            {
                string datos_string = DependencyService.Get<IFileManager>().LeerTexto(Variables.Nombre_Archivo_Offline);
                //    DatosUsuario miObjetoTraidoJSON = JsonConvert.DeserializeObject<DatosUsuario>(datos_string);
                //    bool eliminado = DependencyService.Get<IFileManager>().Eliminar(Variables.Nombre_Archivo_Offline);
                //    Console.WriteLine();
            }

            MainPage = new NavigationPage(new LoginPrincipal());
        }
    }
}