﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace Keeytap
{
    public class LoginPrincipal : ContentPage
    {

        RelativeLayout Principal;
        Entry Nombre_cap;
        Entry Doc_capdor, Nombre_capdor;
        Button botonIniciar;
        Switch localNube;
        Image Capacitacion;
        

        

        public LoginPrincipal()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();

        }

        void CrearVistas()
        {
            

            Capacitacion = new Image


            {

                Source = Core.Capacitacion,

              
         
            

            };

            

            Nombre_cap = new Entry
            {

                Text = "",
                Placeholder = "Nombre capacitación",
                PlaceholderColor = Core.Textos,
                FontSize = 13
               
                


            };

            Nombre_capdor = new Entry
            {

                Text = "",
                Placeholder = "Nombre capacitador",
                PlaceholderColor = Core.Textos,
                FontSize = 13


            };
            Doc_capdor = new Entry
            {
                Text = "",
                Placeholder = "Documento capacitador",
                PlaceholderColor = Core.Textos,
                FontSize = 13,

               
            };

           
            

            localNube = new Switch {
                
                
                IsToggled = true 
            
            
            };

            botonIniciar = new Button
            {
                Text = "Iniciar capacitación",
                BackgroundColor = Core.Botones,
                TextColor = Core.Textos,
                BorderColor = Core.Botones,
                CornerRadius = 20
            };



            Principal = new RelativeLayout { BackgroundColor = Core.Fondos };

            


        }
        void AgregarVistas()
        {
            


            Principal.Children.Add(Capacitacion,
            Constraint.RelativeToParent((p) => { return p.Width * 0.301; }),      // X= POSICION 
            Constraint.RelativeToParent((p) => { return p.Height * 0.164; }),    // Y= POSICION
            Constraint.RelativeToParent((p) => { return p.Width * 0.397; }),      // W= ANCHO DE CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.223; })     // H= ALTO DE
            //Constraint.RelativeToParent((p) => { return p.Width * 0.168; }),
            // Constraint.RelativeToParent((p) => { return 86; }),
            // Constraint.RelativeToParent((p) => { return p.Width * 0.7; }) // Ancho

             );



            Principal.Children.Add(Nombre_cap,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 332; }),
               Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto

               );

            Principal.Children.Add(Nombre_capdor,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 379; }),
              Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto

               );

             Principal.Children.Add(Doc_capdor,
              Constraint.RelativeToParent((p) => { return 64; }),
               Constraint.RelativeToParent((p) => { return 423; }),
               Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
               Constraint.RelativeToParent((p) => { return p.Height * 0.050; }) //Alto
               );




            Principal.Children.Add(localNube,
           Constraint.RelativeToParent((p) => { return p.Width * 0.12; }),      // X= POSICION 
            Constraint.RelativeToParent((p) => { return p.Height * 0.03; }),    // Y= POSICION
            Constraint.RelativeToParent((p) => { return p.Width * 0.834; }),      // W= ANCHO DE CONTROL
            Constraint.RelativeToParent((p) => { return p.Height * 0.0539; })



              );


            Principal.Children.Add(botonIniciar,
            Constraint.RelativeToParent((p) => { return 64; }),
            Constraint.RelativeToParent((p) => { return 533; }),
            Constraint.RelativeToParent((p) => { return p.Width * 0.666; }), // Ancho
            Constraint.RelativeToParent((p) => { return p.Height * 0.065; })//Alto

              );
         

           


            Content = Principal;

        }

        void AgregarEventos()
        {

            //localNube.CheckedChanged += LocalNube_CheckedChanged;
            botonIniciar.Clicked += BotonIniciar_Clicked;

           


        }

     
        private async void BotonIniciar_Clicked(object sender, EventArgs e)
        {

            

            if (string.IsNullOrEmpty(Nombre_cap.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa el nombre de la capacitación", "Aceptar");

            }
            else if (!Doc_capdor.Text.ToCharArray().All(Char.IsDigit))
            {


                await DisplayAlert("¡Aviso!", "El campo documento acepta sólo digitos numéricos", "Aceptar");


            }


            else if (string.IsNullOrEmpty(Nombre_capdor.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa el nombre de capacitador", "Aceptar");

            }

            else if (string.IsNullOrEmpty(Doc_capdor.Text))
            {


                await DisplayAlert("¡Aviso!", "Ingresa el documento de capacitador", "Aceptar");


            }

            //else if (!string.IsNullOrEmpty(Doc_capdor.Text))
            //{

            //    int contador = 0;

            //    for (int i = 0; i < (Doc_capdor.Text).Length; i++)
            //    {
            //        contador++;
            //    }

            //    if (contador <= 6)
            //    {
            //        await DisplayAlert("¡Aviso!", "Tu documento debe contener minimo 8 caracteres", "Aceptar");


            //    }
            //    else
            //    {

            //        await Navigation.PushAsync(new LoginPrincipal());
            //        Navigation.RemovePage(this);

            //    }
            //}
            else
            {

             
                await Navigation.PushAsync(new LoginPrincipal());
                Navigation.RemovePage(this);
                
            }
        }







    }
        //private async void LocalNube_CheckedChanged(object sender, CheckedChangedEventArgs e)
        //{
        //    respuesta = await DisplayAlert("Notificacion", "¿Quieres guardar tu sesión?", "Aceptar", "Cancelar");




        //}



    }
