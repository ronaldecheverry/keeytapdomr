﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keeytap
{
    public class UserData
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
        public string Documento { get; set; }
        public string Peso { get; set; }

    }
}
