﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Keeytap
{
    public static class Core
    {

        //Seccion Colores

        public static Color Navegacion { get; } = Color.FromHex("#C8D3D8");
        public static Color Botones { get; } = Color.FromHex("#FFC267");
        public static Color Textos { get; } = Color.FromHex("#313131");
        public static Color BordeBoton { get; } = Color.FromHex("#C59246");
        public static Color BordeSw { get; } = Color.FromHex("#C8D3D8");

        public static Color Fondos { get; } = Color.FromHex("#C8D3D8");


        // Seccion Images Iconos

        public static ImageSource Capacitacion { get; } = ImageSource.FromFile("Icon_capacitacion.png");

        //public static ImageSource IconoSalir { get; } = ImageSource.FromFile("IconoSalir.png");

        //public static ImageSource IconoAtras { get; } = ImageSource.FromFile("IconoAtras.png");
    }
}