﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Keeytap
{
   
    public class PaginaInicial : ContentPage
    {

        RelativeLayout miVista;
        AnimacionLaunchScreen miAnimacion;
           
        Label labelHola;
        public PaginaInicial()
        {
             NavigationPage.SetHasNavigationBar(this,false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();            
        }

        void CrearVistas() 
        {
            miVista = new RelativeLayout();
            miAnimacion = new AnimacionLaunchScreen();
            labelHola = new Label 
            { 
                Text = "HOLA", 
                FontSize = 64,
                HorizontalTextAlignment = TextAlignment.Center
            };
        }

        void AgregarVistas()
        {
            miVista.Children.Add(labelHola,
               Constraint.RelativeToParent((p) => { return 0; }), // X
               Constraint.RelativeToParent((p) => { return 0; }), // Y
               Constraint.RelativeToParent((p) => { return p.Width * 1; }) // W
           );

            miVista.Children.Add(miAnimacion,
               Constraint.RelativeToParent((p) => { return 0; }), // X
               Constraint.RelativeToParent((p) => { return 0; }), // Y
               Constraint.RelativeToParent((p) => { return p.Width * 1; }), // W
               Constraint.RelativeToParent((p) => { return p.Height * 1; })  // H
           );

            Content = miVista;
        }

        void AgregarEventos()
        {

        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await Task.Delay(1000);
            await miAnimacion.boxAnimation.ScaleTo(14, 800);
            await miAnimacion.boxAnimation.ScaleTo(0, 800);
            await miAnimacion.FadeTo(0,800);
            miAnimacion.IsVisible = false;
        }

    }
}