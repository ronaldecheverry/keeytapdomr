﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Keeytap
{
    public class AnimacionLaunchScreen : RelativeLayout
    {
        public BoxView boxAnimation;
        public AnimacionLaunchScreen()
        {
            boxAnimation = new BoxView
            {
                BackgroundColor = Color.Black,
                CornerRadius = (Variables.ScreenWidth * 0.20) / 2.0,
                Scale = 0
            };
            
            BackgroundColor = Color.White;

            this.Children.Add(boxAnimation,
               Constraint.RelativeToParent((p) => { return p.Width * 0.39; }), // X
               Constraint.RelativeToParent((p) => { return p.Height * 0.44; }), // Y
               Constraint.RelativeToParent((p) => { return p.Width * 0.20; }), // W
               Constraint.RelativeToParent((p) => { return p.Width * 0.20; })  // H
           );
        }
    }
}