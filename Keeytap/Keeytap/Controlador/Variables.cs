﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keeytap
{
    public class Variables
    {
        public static double ScreenWidth { get; set; }
        public static double ScreenHeight { get; set; }
        public static string Nombre_Archivo_Offline { get; set; } = "miArchivo.json";
    }
}
