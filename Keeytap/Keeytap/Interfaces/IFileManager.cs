﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Keeytap
{
    public interface IFileManager
    {
        bool GuardarTexto(string filename, string text, bool overWrite = true);
        string LeerTexto(string filename);
        bool Existe(string filename, string path = "");
        bool Eliminar(string filename, string path = "");
        string TraerRutaDelArchivo();
    }
}
