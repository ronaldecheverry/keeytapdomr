﻿using Keeytap.Droid.Servicios;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

[assembly: Dependency(typeof(FileManagerAndroid))]
namespace Keeytap.Droid.Servicios
{
    public class FileManagerAndroid : IFileManager
    {
        public bool Eliminar(string filename, string path = "")
        {
            string filePath;
            if (path == "")
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                filePath = Path.Combine(path, filename);
            }
            else
            {
                filePath = Path.Combine(path, filename);
            }

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                return true;
            }
            return false;
        }

        public bool Existe(string filename, string path = "")
        {
            var filePath = "";
            if (path == "")
            {
                var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                filePath = Path.Combine(documentsPath, filename);
            }
            else
            {
                filePath = Path.Combine(path, filename);
            }
            return System.IO.File.Exists(filePath);
        }

        public bool GuardarTexto(string filename, string text, bool overWrite = true)
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var filePath = Path.Combine(documentsPath, filename);
            if (!overWrite) if (System.IO.File.Exists(filePath)) return false;
            System.IO.File.WriteAllText(filePath, text);
            return true;
        }

        public string LeerTexto(string filename)
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var filePath = Path.Combine(documentsPath, filename);
            return System.IO.File.ReadAllText(filePath);
        }

        public string TraerRutaDelArchivo()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }
    }
}